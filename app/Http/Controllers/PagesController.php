<?php

namespace App\Http\Controllers;
use App\Restaurants;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\Admin;
use App\User;
use App\Notifications\UserOrderDeliver;
use App\Notifications\AdminOrderDeliver;
use App\Notifications\SMSNotifications;

use Auth;
use Intervention\Image\ImageManagerStatic as Image;


class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    public function addShop($id)
    {
        $shop = Restaurants::where('emp_id', $id)->First();
        // dd($shop);
        if($shop!=null){
            $gotShop =1;
            return redirect()->back()->with('status', 'Shop Already registered');
        }
        else{
            return view('employee.panel.add-shop');
        }
    }

    public function registerShop(Request $request)
    {
        request()->validate([
            'name' =>'required',
            'openhr' => 'required',
            // 'closehr' => 'required|after:'.request('openhr'),
            'closehr' => 'required',
        ]);
            
        Restaurants::create([
            'name' => request('name'),
            'open_hours' => request('openhr'),
            'close_hours' => request('closehr'),
            'emp_id' =>request('emp_id')
        ]);
        // dd($request->all());

        return redirect('/employee')->with('status', 'Shop Added sucessfully');
    }

    public function showproduct($id)
    {
        $products = Product::where('emp_id', $id)->get();
        // dd($products);
        return view('employee.panel.products')->with('products', $products);
    }

    public function addproduct($id){
        $res = Restaurants::where('emp_id', $id)->first();
        if($res !=null){
            $res_id = $res->id;
            // dd($res_id);
            $emp_id = $id;
            return view('employee.panel.add-product')->with('res_id', $res_id )->with('emp_id', $emp_id);
        }else{
            return view('employee.panel.add-shop')->with('status', 'Register Shop First!');
        }

        }

    public function storeproduct(Request $request)
    {
        request()->validate([
            'name' =>'required',
            'details' =>'required',
            'quantity' => 'required',
            'pic' => 'required'
        ]);

        if($request->hasFile('pic')){
            $image = $request->file('pic');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('files/users/'.$filename);
            Image::make($image)->resize(null,768)->save($location);
            $pic = $filename;
        }

        Product::create([
            'name' => request('name'),
            'category' => request('category'),
            'quantity' => request('quantity'),
            'status' => request('status'),
            'pic' => $pic,
            'details' => request('details'),
            'rest_id' => request('res_id'),
            'emp_id' => request('emp_id'),
            'price' => request('price'),
        ]);

        return redirect()->route('employee-home')->with('status', 'Product Added Sucessfully');
    }

    public function viewproduct($id)
    {
        $product = Product::find($id);

        return view('employee.panel.view-product')->with('product', $product);
    }

    public function removeProduct($id)
    {
        $res = Product::where('id', $id)->delete();
        return redirect()->route('employee-home')->with('status', 'Product Removed Successfully');
    }

    public function addStock(Request $request, $id)
    {
        request()->validate([
            'stock' => 'required|min:0',
        ]);

        $product = Product::find($id);

        $product->quantity = $product->quantity + request('stock');
        // dd($product->quantity);
        $product->save();

        return redirect()->back()->with('status','Stock Updated!');
    }

    public function showOrders()
    {
        $emp_id = Auth::user()->id;
        // $orders = Order::where('emp_id', $emp_id)->where('order_status', 'preparing')->get();
        $allorders= Order::where('order_status', 'preparing')->get();
        $orders = [];

        foreach ($allorders as $order) {
            $emps = unserialize($order->emp_id);
            foreach($emps as $emp){
                if($emp == $emp_id){
                    if(!in_array($order , $orders))
                        array_push($orders, $order);
                }
            }
        }

        $myorder =[];
        $myorders = [];
        foreach ($orders as $order) {
            $product_no = explode(',', $order->order_details);
            $order->products =[];
            $myorder= [];
            foreach ($product_no as $pid) {
                if($pid != ""){
                    $product = Product::find($pid);
                    if(!in_array($product , $myorder))
                        array_push($myorder, $product);
                }
            }
            array_push($myorders, $myorder);
            $order->products = $myorders;
            // dd($order->products);
            // array_push($order->products, $myorder);
        }        

        return view('employee.panel.showorders')->with('orders', $orders);
    }

    public function deliver($id)
    {
        $order = Order::where('order_no',$id)->first();
        // dd($order);
        $order->order_status = 'delivering';
        $order->save();

        $admin = Admin::all()->first();
        $user_id = $order->user_id;
        $user = User::find($user_id);


        \Notification::send($admin, new AdminOrderDeliver());
        \Notification::send($admin, new SMSNotifications('Dear Admin, an order has been delivered Order ID: ', $order->order_no));


        \Notification::send($user, new UserOrderDeliver());
        \Notification::send($user, new SMSNotifications('Dear User your order is being delivered. Order ID: ', $order->order_no));


        return redirect()->back()->with('status','Order is Being Delivered!');
    }

    public function editproduct($id)
    {
        $product = Product::find($id);

        return view('employee.panel.update-product')->with('product', $product);
    }

    public function updateproduct(Request $request, $id)
    {        
        request()->validate([
            'name' =>'required',
            'category' =>'required',
            'details' =>'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);

        $product = Product::find($id);

        if($request->hasFile('pic')){
            $image = $request->file('pic');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('files/users/'.$filename);
            Image::make($image)->resize(null,768)->save($location);
            $pic = $filename;

            $oldpicname = $product->pic;
            Storage::delete($oldpicname);
        }else{
            $pic = $product->pic ;
        }

        $product->update([
            'name' => request('name'),
            'category' => request('category'),
            'quantity' => request('quantity'),
            'pic' => $pic,
            'details' => request('details'),
            'price' => request('price'),
        ]);

        return redirect()->route('employee-home')->with('status', 'Product Updated Sucessfully');
    }
}

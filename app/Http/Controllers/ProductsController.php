<?php

namespace App\Http\Controllers;
use App\Product;
use App\Notifications\OrderPlaced;
use Illuminate\Http\Request;
use Session;
use App\Cart;
use App\Order;
use Auth;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::where('status', 'approved')->where('quantity', '>', 0)->get();
        return view('products')->with('products', $products);
    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        // dd($oldCart->items);

        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        // dd($cart);
        $request->session()->put('cart', $cart);
        return redirect()->route('products')->with('message', 'Product has been added to the cart!');
    }

    public function getCart()
    {
        if(!Session::has('cart')){
            return view('shopping-cart');
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        
        return view('shopping-cart', ['products'=> $cart->items, 'totalPrice'=>$cart->totalPrice]);
    }

    public function getCheckout()
    {
        if(!Session::has('cart')){
            return view('shopping-cart');
        }
        $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $total = $cart->totalPrice;
            return view('checkout')->with('total',$total);
    }

    public function getReduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);

        if(count($cart->items) > 0){
            Session::put('cart', $cart);
        }else{
            Session::forget('cart');
        }
        return redirect()->route('product.shoppingCart')->with('message', 'A Product has been removed from the cart!');
    }

    public function getRemoveItem($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        if(count($cart->items) > 0){

            Session::put('cart', $cart);
        }else{
            Session::forget('cart');
        }

        return redirect()->route('product.shoppingCart')->with('message', 'Products has been removed from the cart!');

    }
}

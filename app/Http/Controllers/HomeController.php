<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\User;
use App\Employee;
use Illuminate\Support\Facades\Storage;

use App\Order;
use App\Product;
use Hash;
use Session;
use App\Admin;
use App\Notifications\OrderPlaced;
use App\Notifications\SMSNotifications;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::guard()->user()->id;
        $details = User::find($id);

        return view('index')->with('details',$details);
    }

    public function showorders()
    {
        return view('orders');
    }

    public function postcheckout(Request $request)
    {
        request()->validate([
            'card_num' => 'numeric',
            'cvc' => 'numeric',
        ]);

        $admin = Admin::all()->first();
        $order_id= Str::random(12);
        $cart = Session::get('cart');
        $products_id = "";
        $emp_id= [];
        $preqty = [];

        //Reducing Item Quantity from DB
        foreach ($cart->items as $item) {
            $qty = $item['qty'];
            $product = Product::find($item['item']['id']);
            array_push($preqty, $qty);
            // $product->prequantity += $qty;
            $product->quantity -= $qty;
            $product->save();
        }

        //get all employeesid from cart
        foreach ($cart->items as $items) {
            $products_id .= $items['item']['id']." ,";
            $emp = $items['item']['emp_id'];
            if(!in_array($emp , $emp_id))
                array_push($emp_id, $emp);
        }

        $serializedArr = serialize($emp_id);
        $serializedquantity = serialize($preqty);

        // dd($cart->totalPrice);
        Order::create([
            'order_no' => $order_id,
            'order_details' => $products_id,
            'order_date' => date('Y-m-d'),
            'order_time' => date('H:i:s'),
            'order_status' => 'processing',
            'category' => 'FOOD',
            'price' => $cart->totalPrice,
            'emp_id' => $serializedArr,
            'prequantity' => $serializedquantity,
            'user_id' => Auth::user()->id,
        ]);

        // dd('Done');
        \Notification::send($admin, new OrderPlaced($order_id));
        $user = User::find(Auth::user()->id);
        \Notification::send($user,new SMSNotifications('Dear user, your order has been placed. Order ID: ', $order_id));
        \Notification::send($admin,new SMSNotifications('Dear Admin, an order has been placed. Order ID: ', $order_id));

        Session::forget('cart');
        return view('Thanks')->with('order_id', $order_id);
    }

    public function dashboard($id)
    {
        $details = User::find($id);
        // dd($details);
        return view('auth.dashboard')->with('details', $details);
    }

    public function userorders()
    {
        $orders = Order::where('user_id', Auth::user()->id)->get();
        return view('user-orders')->with('orders', $orders);
    }

    public function trackorders($id)
    {
        $order = Order::where('order_no', $id)->first();
        return view('user-trackorders')->with('order', $order);
    }

    public function changepassform()
    {
        return view('user-changepassword');
    }

    public function changepass(Request $request)
    {
        $id = Auth::user()->id;
        $user = User::find($id);

        
        request()->validate([
            'oldpass' =>'required',
            'password' =>'required|min:8',
            'password_confirmation' => 'required|min:8',
        ]);
        

        if(Hash::check($request->oldpass , $user->password)){   //Check if current pasword match
            if(!Hash::check($request->password , $user->password)){ //Check if current and new password is not same
                $confirm =Hash::make($request->password_confirmation);
                if(Hash::check($request->password , $confirm)){   //Check if new and confirm are same
                    $user->password = Hash::make($request->password);
                    $user->save(); 
                    return redirect()->back()->with('status','Password Changed Successfully!');
                }else{
                    return redirect()->back()->with('status','New password and Confirmation password are not Same!');
                }
            }
            else{
                return redirect()->back()->with('status','New password cannot be same as Current Password!');
            }            
        }
        else{
            return redirect()->back()->with('status','Current password is Incorrect!');
        }
        
    }

    public function accountdetails($id)
    {
        $user = User::find($id);
        return view('user-account')->with('user', $user);
    }

    public function updatedetails(Request $request)
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);

        request()->validate([
            'num' => ['required', 'string','starts_with:+92'],
        ]);

        if($request->hasFile('pic')){
            $image = $request->file('pic');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('files/users/'.$filename);
            // dd($location);
            Image::make($image)->resize(null,768)->save($location);
            $pic = $filename;
            $oldpicname = $user->pic;
            if($oldpicname != 'default_user.png'){
                Storage::delete($oldpicname);
            }
        }else{
            $pic = $user->pic;
        }

        $user->update([
            'name' => request('name'),
            'email' => request('email'),
            'address' => request('addr'),
            'number' => request('num'),
            'city' => request('city'),
            'pic' => $pic,
        ]);

        return redirect()->back()->with('status', 'Details Updated Succesfully!');
    }

    public function cancelOrder($id)
    {
        // dd('helo');
        $order = Order::where('order_no', $id)->first();
        // dd($order);
        $order->order_status = 'canceled';
        $order->save();
        $user = User::find($order->user_id);
        $order_id = $order->order_no;
        \Notification::send($user,new SMSNotifications('Dear user your order has been Cancelled. Order ID: ', $order_id));

        return redirect()->back()->with('message', 'Order Canceled');
    }
}

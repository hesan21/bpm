<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Employee;

class EmployeeLoginController extends Controller
{
    
    public function __construct(){
        $this->middleware('guest:employee', ['except'=> ['logout']]);
    }

    public function showLoginForm(){
        return view('employee.auth.login');
    }

    public function login(Request $request){

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        // dd($request->all());
        if(Auth::guard('employee')->attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember)){
            
            return redirect()->intended(route('employee-home'));
        }

        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('employee')->logout();
        return redirect('/');
    }
}

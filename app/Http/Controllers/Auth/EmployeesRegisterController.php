<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use \App\Employee;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeesRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/employee';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:employee');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'num' => ['required', 'string','starts_with:+92'],
        ]);
    }

    public function showRegisterForm(){
        return view('employee.auth.register');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = Employee::create([
            'name' => request('name'),
            'email' => request('email'),
            'number' => request('num'),
            'password' => Hash::make($request['password']),
        ]);

        // $this->guard('employee')->login($user);

        // return $this->registered($request, $user)
        //                 ?: redirect($this->redirectPath());
        
        return redirect(route('employee-home'));

    }
}

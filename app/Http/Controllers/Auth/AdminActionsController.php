<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\Employee;
use App\Product;
use App\Order;
use App\Restaurants;

use Auth;
use App\Http\Controllers\Controller;
use App\Notifications\OrderApproved;
use App\Notifications\UserOrderApproved;
use App\Notifications\UserOrderRejected;
use App\Notifications\UserOrderComplete;
use App\Notifications\EmployeeOrderComplete;
use App\Notifications\SMSNotifications;

use Illuminate\Http\Request;

class AdminActionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function users()
    {
        $users = User::get();
        // dd($users);
        return view('admin.pages.manage-user')->with('users', $users);
    }

    public function showuser($id)
    {
        $user = User::find($id);
        // dd($user);
        return view('admin.pages.showuser')->with('user', $user);
    }

    public function deleteuser($id)
    {
        Order::where('user_id', $id)->delete();

        User::find($id)->delete();

        return redirect()->back()->with('status','User account has been deleted!');
    }

    public function employees()
    {
        $users = Employee::get();
        // dd($users);
        return view('admin.pages.manage-employee')->with('users', $users);
    }

    public function showemployee($id)
    {
        $user = Employee::find($id);
        // dd($user);
        return view('admin.pages.showemployee')->with('user', $user);
    }

    public function deleteemployee($id)
    {
        Product::where('emp_id', $id)->delete();
        Restaurants::where('emp_id', $id)->delete();
        Employee::find($id)->delete();

        return redirect()->back()->with('status','Employee account has been deleted!');
    }

    public function showapproval()
    {
        $products = Product::where('status', 'processing')->get();
        // dd($products);
        return view('admin.pages.products-approval')->with('products', $products);
    }

    public function viewProduct($id){
        $product = Product::find($id);
        $rest = Restaurants::find($product->rest_id);
        // dd($rest);
        
        return view('admin.pages.admin-view-product')->with('product', $product)->with('rest', $rest);
    }

    public function productapproved($id)
    {
        $product = Product::find($id);
        // dd($product);
        $product->status = "approved";
        $product->save();

        return redirect()->route('products-approval')->with('status', 'Product has been approved!');
    }

    public function productreject($id)
    {
        $product = Product::find($id);
        // dd($product);
        $product->status = "rejected";
        $product->save();
        return redirect()->route('products-approval')->with('status', 'Product has been approved!');
    }

    public function showApprovals()
    {
        $orders = Order::where('order_status', 'processing')->get();
        // dd($orders[0]);

        foreach($orders as $order){
            $details = $order->order_details;
            $product_no = explode(',', $details);
            $pdetail = [];
            foreach ($product_no as $id) {
                if($id != ""){
                    array_push($pdetail, Product::find($id));
                }
            }

            $order->order_details = $pdetail;
        }
        return view('admin.pages.approvals')->with('orders', $orders);
    }

    public function orderview($id)
    {
        $order = Order::where('order_no', $id)->first();
        $user = User::find($order->user_id);
 
            $details = $order->order_details;
            $product_no = explode(',', $details);
            $products = [];
            foreach ($product_no as $id) {
                if($id != ""){
                    array_push($products, Product::find($id));
                }
            }

        return view('admin.pages.order-view')->with('order', $order)->with('products', $products)->with('user', $user);
    }

    public function orderapproved($id)
    {
        $order =Order::find($id);

        $details = $order->order_details;
        $product_no = explode(',', $details);
        $pdetail = [];
        foreach ($product_no as $id) {
            if($id != ""){
                array_push($pdetail, Product::find($id)->emp_id);
                $product = Product::find($id);
                $product->save();
            }
        }
        $emp = [];

        foreach ($pdetail as $detail) {
            if(!in_array(Employee::find($detail) , $emp))
                array_push($emp, Employee::find($detail));
        }
        
        // dd($emp);
        $order->order_status = 'preparing';
        $order->save();

        $order_id = $order->order_no;
        
        //A notification to the employee
        \Notification::send($emp, new OrderApproved());
        \Notification::send($emp, new SMSNotifications('Dear Employee, an order has been placed. Order ID: ', $order_id));
        
        $user = User::find($order->user_id);
        \Notification::send($user, new UserOrderApproved());
        \Notification::send($user, new SMSNotifications('Dear user, your order has been Processed. Order ID: ', $order_id));

        return redirect()->back()->with('message', 'Order Approved!');
    }

    public function orderreject($id)
    {
        $order =Order::find($id);
        
        $details = $order->order_details;
        $product_no = explode(',', $details);
        $pdetail = [];
        foreach ($product_no as $id) {
            if($id != ""){
                array_push($pdetail, Product::find($id));
            }
        }
        $counter = 0;
        $pqty = unserialize($order->prequantity);

        foreach($pdetail as $product){
            $product->quantity += $pqty[$counter];
            // dd($product->quantity);
            unset($pqty[$counter]);
            // dd($pqty);

            $order->prequantity = count($pqty) > 0 ? serialize($pqty) : null;
            $counter++;
            $product->save();
        }

        $order->order_status = 'rejected';
        $order->save();
        $order_id = $order->order_no;
        $user = User::find($order->user_id);
        \Notification::send($user, new UserOrderRejected());
        \Notification::send($user, new SMSNotifications('Dear user your order has been Canceled. Order ID: ', $order_id));

        return redirect()->back()->with('message', 'Order Rejected!');
    }

    public function orderstatus()
    {
        $orders = Order::where('order_status', 'delivering')->get();
        // dd($orders[0]);
        return view('admin.pages.orders')->with('orders', $orders);        
    }

    public function ordercomplete($id)
    {
        $order = Order::where('order_no', $id)->first();
        $order->order_status = 'delivered';
        $order->save();
        $order_id = $order->order_no;
        $user = User::find($order->user_id);   
        $emps = unserialize($order->emp_id);
        $employees = [];
            foreach($emps as $emp){
                if(!in_array($emp , $employees))
                    array_push($employees, $emp);
            }  
            foreach ($employees as $employee) {
                $empl = Employee::find($employee);
                \Notification::send($empl, new EmployeeOrderComplete()); 
                // \Notification::send($empl, new SMSNotifications('Dear Employee your order has been Delivered Successfully. Order ID: ', $order_id));
                  
            }

        \Notification::send($user, new UserOrderComplete());
        \Notification::send($user, new SMSNotifications('Dear user your order has been Delivered. Order ID: ', $order_id));

        return redirect()->route('order-status')->with('message', 'Order Successfully Completed!');
    }


}

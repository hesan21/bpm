<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Employee;

class EmployeesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $id = Auth::guard()->user()->id;
        // $details = User::find($id);

        return view('employee.home');
    }
    

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Restaurants;
use Auth;
use App\Mail\Contacted;

class MainPagesController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function viewProduct($id){
        $product = Product::find($id);
        $rest = Restaurants::find($product->rest_id);
        // dd($rest);
        
        return view('view-product')->with('product', $product)->with('rest', $rest);
    }

    public function viewShops()
    {
        $shops = Restaurants::get();
        // dd($shops);
        return view('shops')->with('shops', $shops);
    }

    public function sendMail(Request $request)
    {
        request()->validate([
            'name' =>'required',
            'email' =>'required',
            'message' =>'required',
        ]);
        $data =[
            'email'   => request('email'), 
            'subject' => 'Contact',
            'body'    => request('message')
        ];

        \Mail::send('contactus', $data, function($message) use ($data)
        {
            $message->from($data['email']);
            $message->to('admin@bpm.com');
            $message->subject($data['subject']);
        });

        return redirect()->back()->with('message', 'Mail Sent!');
    }

    public function shopsProduct($id)
    {
        $rest = Restaurants::find($id);
        $products = Product::where('rest_id', $rest->id)->get();
        // dd($products);
        return view('shops-products')->with('products', $products)->with('rest', $rest);
    }

}

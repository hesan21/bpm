<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\EmployeeResetPasswordNotification;


class Employee extends Authenticatable 
{
    use Notifiable;

    protected $guard = 'employee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new EmployeeResetPasswordNotification($token));
    }

    // public function routeNotificationForNexmo($notification)
    // {
    //     return $this->number;
    // }
}

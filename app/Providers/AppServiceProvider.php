<?php

namespace App\Providers;
use App\Product;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if(Schema::hasTable('products')){
            $products = Product::where('status', 'approved')->where('quantity', '>', 0)->get()->take(3);
            // dd($products);
            View::share('products', $products);
        }
    }
}

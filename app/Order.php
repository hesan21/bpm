<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded =[];

    public function routeNotificationForNexmo($notification)
    {
        return $this->order_no;
    }
}

@extends('layouts.app')

@section('title')
    About US
@endsection

@section('content')
    <div class="container mt-4">
        <h2 class="text-center my-5">About US</h2>
        <h5 class="text-center col-8 offset-2 mb-3" style="line-height: 5vh">
            “Business Process Management” is a web  application that can be used to alerts and notifications during processing product. It shows the whole process of the product during request to deliver. The end-user can search and avail that product. After selecting product, user can request the product and Before requesting user must has login. After login, user can request the product , system will generate notification and alert to notify automatically admin about the user request for product. When processing system will start, step by step system will show status and  notify both the end-user and admin via SMS alert or email. System will update every detail of product` in admin record.
        </h5>
    </div>
    @include('includes.footer')
@endsection
@extends('layouts.app')

@section('title')
    Shop
@endsection

@section('content')
    <div class="container head-image-shops">
        <div class="centered"><h1 class="display-4 text-light px-2 py-2">All Shops</h1></div>
    </div>

    <div class="grid" id="mycard">
        <figure class="effect-zoe">
            <img src="https://static.pexels.com/photos/10664/b4d29be0fa6c806b5239556952d2e24d-large.jpg" alt="img26"/>
            <figcaption>
                <h2>Creative <span>Zoe</span></h2>
                <button class="btn btn-sm btn-success"> Products <i class="fas fa-angle-double-right"></i></button>
            </figcaption>			
        </figure>
        <figure class="effect-zoe">
            <img src="https://static.pexels.com/photos/10664/b4d29be0fa6c806b5239556952d2e24d-large.jpg" alt="img26"/>
            <figcaption>
                <h2>Creative <span>Zoe</span></h2>
                <button class="btn btn-sm btn-success"> Products <i class="fas fa-angle-double-right"></i></button>
            </figcaption>			
        </figure>
        <figure class="effect-zoe">
            <img src="https://static.pexels.com/photos/10664/b4d29be0fa6c806b5239556952d2e24d-large.jpg" alt="img26"/>
            <figcaption>
                <h2>Creative <span>Zoe</span></h2>
                <button class="btn btn-sm btn-success"> Products <i class="fas fa-angle-double-right"></i></button>
            </figcaption>			
        </figure>
        <figure class="effect-zoe">
            <img src="https://static.pexels.com/photos/10664/b4d29be0fa6c806b5239556952d2e24d-large.jpg" alt="img26"/>
            <figcaption>
                <h2>Creative <span>Zoe</span></h2>
                <button class="btn btn-sm btn-success"> Products <i class="fas fa-angle-double-right"></i></button>
            </figcaption>			
        </figure>
    </div>
    <div class="clear-float"></div>
    @include('includes.footer')
@endsection
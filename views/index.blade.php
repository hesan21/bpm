@extends('layouts.app')

@section('title')
    Business Process Management
@endsection

@section('content')
    @include('includes.carousel')
    <h2 class="text-center">------ Top Products ------</h2>
    @include('includes.product-cards')
    <div class="clear-float"></div>
    <div>
        <a href="" class="nav-link"><h2 class="text-center my-5">------ See All Products ------</h2></a>
    </div>

    @include('includes.footer')
@endsection

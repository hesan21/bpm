@extends('layouts.app')

@section('title')
    User Login
@endsection

@section('content')
    <div class="text-center mb-4">
        <img class="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Sign In</h1>
    </div>

    <form class="form-signin mb-5" method="POST" action="{{ route('login') }}">
        @csrf
                
        <div class="form-label-group">
            
            <input type="email" id="inputEmail" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" placeholder="Email address" required autofocus>
            <label for="inputEmail">Email address</label>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        
        <div class="form-label-group">
            
            <input type="password" id="inputPassword" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password"  autofocus>
            <label for="inputPassword">Password</label>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        
        <div class="checkbox mb-3 ml-3">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label" for="remember">
                {{ __('Remember Me') }}
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
        
        @if (Route::has('password.request'))
            <a class="btn btn-link col-12" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif

        <a href="{{route('user-signup')}}" class="btn btn-link col-12">Dont have an account? Sign Up</a>
    </form>

    @include('includes.footer')
@endsection
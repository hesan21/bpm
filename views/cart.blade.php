@extends('layouts.app')

@section('title')
    Shopping Cart
@endsection

@section('content')
    <div class=" container mt-2">
        <h2 class=" text-left my-4">Shopping Cart</h2>

        <div class="card-group col-12">
            <div class="card col-12 col-lg-7">
                <div class="card-body">
                    <h5 class="text-center">Product Detail</h5>
                </div>
            </div>
            <div class="card d-none d-lg-flex col-lg-3">
                <div class="card-body">
                    <h5 class="text-center">Action</h5>
                </div>
            </div>
            <div class="card d-none d-lg-flex col-lg-2">
                <div class="card-body">
                    <h5 class="text-center">Quantity</h5>
                </div>
            </div>
        </div>

        <div class="card-group col-12">
            <div class="card col-12 col-lg-7">
                <div class="row card-body">
                    <a href="" class="d-none d-sm-flex col-sm-4 col-lg-5"><img src="{{asset('imgs/shops.jpg')}}" alt="" class="col-12"></a>
                    <p class="col-12 d-sm-flex col-sm-6 col-lg-5">
                        <a href=""> Product Name</a>
                    </p>
                </div>
            </div>
            <div class="card d-none d-lg-flex col-lg-3">
                <div class="card-body">
                    <ul class="navbar nav d-lg-inline-block">
                        <li class="nav-link"><a href="">Update Quantity <i class="fas fa-pen"></i></a></li>
                        <li class="nav-link"><a href="">Move to Wishlist <i class="fas fa-heart"></i></a></li>
                        <li class="nav-link"><a href="">Remove Item <i class="fas fa-trash"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card d-none d-lg-flex col-lg-2">
                <div class="card-body">
                    <form action="">
                        <input type="number" name="item-quantity" class="col-lg-12">
                    </form>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('title')
    Products
@endsection

@section('content')
    <div class="container head-image-products">
        <div class="centered"><h1 class="display-4 text-light px-2 py-2">All Products</h1></div>
    </div>

    <div class="grid" id="mycard">
        <figure class="effect-zoe">
            <img src="https://static.pexels.com/photos/10664/b4d29be0fa6c806b5239556952d2e24d-large.jpg" alt="img26"/>
            <figcaption>
                <h2>Creative <span>Zoe</span></h2>
                <p class="icon-links">
                    @include('includes.add-to-cart')</a>
                    <a href="#" ><i class="fas fa-heart"></i></a>
                    <a href="#" ><i class="far fa-eye px-2"></i></a>
                </p>
            </figcaption>			
        </figure>
        
    </div>
    <div class="clear-float"></div>
    @include('includes.footer')
@endsection
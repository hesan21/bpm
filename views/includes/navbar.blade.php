<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
  <div class="container mt-2">
    <a class="navbar-brand mr-5" href="/">Business Process Management</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample04">
        <ul class="navbar-nav mr-auto ml-5">
            <li class="nav-item ">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href={{route('shops')}}>Shops</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href={{route('products')}}>Products</a>
            </li>
            <li class="nav-item mr-5">
                <a class="nav-link" href={{route('contact')}}>Contact</a>
            </li>
            <form role="search" id="search">
                <div class="row">
                    <input type="text" class="form-control col-8 col-sm-10" placeholder="Search">
                    <button type="submit" class="form-control col-2 col-sm-2 form-control-submit"><i class="fas fa-search"></i></button>
                </div>
            </form>
            
            
        </ul>
        
        <div class="navbar-right">
            <ul class="navbar-nav ml-5">
                <li class="nav-item mr-1"><a class="nav-link" href="{{route('login')}}"><i class="fas fa-user"></i></a></li>
                <li class="nav-item mr-1"><a class="nav-link" href="{{route('register')}}"><i class="fas fa-user-plus"></i></i></a></li>
                <li class="nav-item mr-1"><a class="nav-link" href="{{route('cart')}}"><i class="fas fa-shopping-cart"></i>1</a></li>
            </ul>
    </div>
  </div>
</nav>

<div class="clearfix"></div>

@extends('layouts.app')

@section('content')
    <div class="container mt-3">
        <h5 class="text-center display-4">Thank you for shopping!</h5>
        <h5 class="text-center">Your Order has been placed</h5>
        <div class="text-center text-success"><i class="far fa-2x fa-check-circle"></i></div>
        <p class="text-center">Your Order ID: <strong> {{$order_id}} </strong></p>
        <div class="text-center"><small>(You can use your Order ID to track your orders)</small></div>
    </div>
@endsection
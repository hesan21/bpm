@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">My Account</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-deck mt-4">
                            <div class="card col-12 col-sm-12 col-md-3 col-lg-4">
                                <div class="card-body text-primary">
                                    <ul class="navbar-nav">
                                        <li><a href="{{route('orders')}}">My Orders</a></li>
                                        <li><a href="">Change Password</a></li>
                                        <li><a href=""></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card col-12 col-sm-12 col-md-9 col-lg-8">
                                <div class="card-header">Header</div>
                                <div class="card-body text-primary">
                                    <h5 class="card-title">Primary card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

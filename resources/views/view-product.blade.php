@extends('layouts.app')

@section('content')
    <div class="container mt-2">
            <a href="{{route('product.back')}}" class="btn btn-success m-2"><i class="fas fa-angle-double-left"></i> All products</a>
        
        <div class="card col-12">
            <img class="w-100" src="{{asset('/files/users/'.$product->pic)}}">
        </div>
        <div class="card col-12">
            <h2 class="display-4">{{$product->name}}</h2>
            <small>by <strong>{{$rest->name}}</strong> Restaurant</small>
            <p class="mt-2">{{$product->details}}}</p>
            <p>Available Quantity: {{$product->quantity}}</p>
            <h5 class="text-h5rimary">Price: {{$product->price}}</p>
            <a href="{{route('product.addToCart', ['id' => $product->id])}}" class="btn btn-primary"><i class="fas fa-shopping-cart px-1"></i> Add to Cart</a>
        </div>
    </div>
@endsection
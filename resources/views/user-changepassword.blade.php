@extends('layouts.app')

@section('title')
    Chnage Password
@endsection

@section('content')
    @include('includes.user-dash')
        <div class="card col-12 col-md-9">
            <div class="card-header text-center">
                Change Password
            </div>
            @if(Session::has('status'))
            <div class="alert alert-danger text-center mt-2">
                {{session('status')}}
            </div>
            @endif
            <div class="card-body">
                <form method="POST" action="{{route('user.changepasssubmit')}}">
                    @csrf

                    <div class="form-group row">
                        <label for="oldpass" class="col-md-4 col-form-label text-md-right">{{ __('Current Password') }}</label>

                        <div class="col-md-6">
                            <input id="oldpass" type="password" class="form-control @error('oldpass') is-invalid @enderror" name="oldpass" required autofocus>

                            @error('oldpass')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password_confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm New Password') }}</label>

                        <div class="col-md-6">
                            <input id="password_confirm" type="password" class="form-control" name="password_confirmation" required>

                            @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Change Password') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
@endsection
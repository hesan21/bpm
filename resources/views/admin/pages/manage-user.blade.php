@extends('admin.layouts.auth')

@section('content')
    <div class="container col-12 col-sm-9">
        <div class="card">
            <div class="card-header text-center">
                Manage Users
            </div>
            @if(Session::has('status'))
                <div class="alert alert-success">{{session('status')}}</div>
            @endif
            <div class="card-body">
                @if(!$users->isEmpty())
                    <ul class="navbar-nav">
                        @foreach ($users as $user)
                            <li class="nav-items">
                                <a href="{{route('user-manage', $user->id)}}">{{$user->name}}</a>
                                <a href="{{route('admin.deleteuser', $user->id)}}" class="float-right btn btn-danger">Delete User Account</a>
                                <a href="{{route('user-manage', $user->id)}}" class="float-right btn btn-secondary">View Details</a>                                

                            </li> 
                            <hr> 
                        @endforeach
                    </ul>
                @else
                    <div class="text-danger text-center">
                        No users
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@extends('admin.layouts.auth')

@section('content')
    <div class="container mt-2">
        <div class="card col-12">
            <img class="w-100" src="{{asset('/files/users/'.$product->pic)}}">
        </div>
        <div class="card col-12">
            <h2 class="display-4"> {{$product->name}}</h2>
            <small>by <strong>{{$rest->name}}</strong> Restaurant</small>
            <p class="mt-2">{{$product->details}}}</p>
            <p>Available Quantity: {{$product->quantity}}</p>
            <h5 class="text-h5rimary">Price: {{$product->price}}</p>

            <a href="{{route('product-approved', $product->id)}}" class="btn btn-success">Approve</a>
            <a href="{{route('product-reject',$product->id)}}" class="btn btn-danger">Reject</a>
        </div>
    </div>
@endsection
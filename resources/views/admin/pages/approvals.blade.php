@extends('admin.layouts.auth')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header text-center">
                Orders Approval
            </div>
            @if(Session::has('message'))
                <div class="alert alert-danger text-center mt-2">
                    {{session('message')}}
                </div>
            @endif
            <ul class="card-body">
                @if(!$orders->isEmpty())
                    <li class="list-group-item">
                        <h5 class="col-3 col-sm-4 d-inline-block">Order No</h5>
                        <h5 class="col-3 col-sm-4 d-inline-block">Product(s)</h5>
                        <h5 class="col-2 col-sm-3 d-inline-block">Availability</h5>
                    </li>
                    @foreach ($orders as $order)
                        <li class="list-group-item">
                            <p class="d-inline-block col-3 col-sm-4"><a href="{{route('admin.order.view', $order->order_no)}}">{{$order->order_no}}</a></p>
                            <p class=" d-inline-block col-3 col-sm-4">
                                @foreach($order->order_details as $detail)
                                    {{$detail->name}},
                                @endforeach
                            </p>
                            <p class="d-inline-block col-1 badge">
                                @foreach($order->order_details as $detail)
                                    {{$detail->quantity>0 ? 'InStock' : 'Unavailable'}},
                                @endforeach
                            </p>
                            <div class="d-md-inline-block ml-md-5">
                                <a href="{{route('order-approved', $order->id)}}" class="btn btn-success">Approve</a>
                                <a href="{{route('order-reject', $order->id)}}" class="btn btn-danger">Reject</a>
                            </div>
                        </li>
                    @endforeach
                @else
                    <li class="list-group-item alert-danger text-center">No Order requires approval</li>
                @endif
            </ul>
        </div>
    </div>
@endsection
@extends('admin.layouts.auth')

@section('content')
    @if(Session::has('status'))
        <div class="col-8 offset-2 alert alert-success">
            {{session('status')}}
        </div>
    @endif

    @if(!$products->isEmpty())
        <ul class=" col-8 offset-2 list-group-items">
            @foreach ($products as $product)
                <li class="list-group-item">
                    <a href="{{route('admin.product.view', $product->id)}}">{{$product->name}}</a>
                    <div class="float-right">
                        <a href="{{route('product-approved', $product->id)}}" class="btn btn-success">Approve</a>
                        <a href="{{route('product-reject',$product->id)}}" class="btn btn-danger">Reject</a>
                    </div>
                </li>

            @endforeach
        </ul>
    @else
        <div class="container">
            <h2 class="text-center">
                No New Product!
            </h2>
        </div>
    @endif
@endsection
@extends('admin.layouts.auth')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header text-center">
                Orders Delivering Status
            </div>

            @if(Session::has('message'))
                <div class="alert alert-danger text-center mt-2">
                    {{session('message')}}
                </div>
            @endif

            <ul class="card-body">
                @if(!$orders->isEmpty())
                    <li class="list-group-item">
                        <h5 class="col-3 col-sm-4 d-inline-block"><strong>Order No</strong></h5>
                        <h5 class="col-3 col-sm-4 d-inline-block"><strong>Status</strong></h5>
                    </li>
                    @foreach ($orders as $order)
                    {{-- {{dd($order)}} --}}
                        <li class="list-group-item">
                            <p class="d-inline-block col-3 col-sm-4"><a href="{{route('admin.order.view', $order->order_no)}}">{{$order->order_no}}</a></p>
                            <p class=" d-inline-block col-3 col-sm-4">
                                {{$order->order_status}}
                            </p>
                            
                            <div class="d-md-inline-block ml-md-5">
                                <a href="{{route('order-complete', $order->order_no)}}" class="btn btn-success">Mark Complete</a>
                            </div>
                        </li>
                    @endforeach
                @else
                    <li class="list-group-item alert-danger text-center">No Order is being delivered at the moment</li>
                @endif
            </ul>
        </div>
    </div>
@endsection
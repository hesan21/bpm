@extends('admin.layouts.auth')

@section('content')
    <div class="container">
        @if( $user ?? 'not-exist' )
            <div class="card">
                <div class="card-header text-center">
                    Employee Detail
                </div>
                <div class="card-body row">
                    
                    <div class="col-8 mt-3">
                        <p><strong>EmployeeID:</strong> {{$user->id}}</p>
                        <p><strong>Employee Name:</strong> {{$user->name}}</p>
                        <p><strong>Email:</strong> {{$user->email}}</p>
                        <p><strong>Number:</strong> {{$user->number}}</p>
                    </div>
                </div>
            </div>
        @else
            <div class="text-center">
                No such User!
            </div>
        @endif
    </div>
@endsection
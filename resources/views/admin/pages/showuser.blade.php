@extends('admin.layouts.auth')

@section('content')
    <div class="container">
        @if( $user ?? 'not-exist' )
            <div class="card">
                <div class="card-header text-center">
                    User Detail
                </div>
                <div class="card-body row">
                    <div class="col-4">
                        <img class="w-100" src="{{asset('files/users/'.$user->pic)}}">
                    </div>
                    <div class="col-8 mt-3">
                        <p><strong>userID:</strong> {{$user->id}}</p>
                        <p><strong>User Name:</strong> {{$user->name}}</p>
                        <p><strong>Email:</strong> {{$user->email}}</p>
                        <p><strong>Address:</strong> {{$user->address}}</p>
                        <p><strong>Number:</strong> {{$user->number}}</p>
                        <p><strong>City:</strong> {{$user->city}}</p>
                    </div>
                </div>
            </div>
        @else
            <div class="text-center">
                No such User!
            </div>
        @endif
    </div>
@endsection
@extends('admin.layouts.auth')

@section('content')
    <div class="container">
        @if( $user ?? 'not-exist' )
            <div class="card">
                <div class="card-header text-center">
                    Order Details
                </div>
                <div class="card-body row">
                    
                    <div class="col-12 mt-3">
                        <p><strong>Order ID:</strong> {{$order->order_no}}</p>
                        <p><strong>Order Date:</strong> {{$order->order_date}}</p>
                        <p><strong>Order Time:</strong> {{$order->order_time}}</p>
                        <p><strong>Order by:</strong> 
                            <a href="{{route('user-manage', $user->id)}}">{{$user->name}}</a>
                        </p>
                        @if($order->order_status == 'preparing')
                            <a href="{{route('order-approved', $order->id)}}" class="btn btn-success">Approve Order</a>
                            <a href="{{route('order-reject', $order->id)}}" class="btn btn-danger">Reject Order</a>
                        @elseif($order->order_status == 'delivering')
                            <a href="{{route('order-complete', $order->order_no)}}" class="btn btn-success">Mark Complete</a>
                        @endif
                        <div class="card-header my-2 text-center">
                            Products
                        </div>
                            @foreach ($products as $product)
                                <div class="row" class="col-12 ">
                                    <img class="col-5 mb-3" style="width:50%; height:150px;" src="{{asset('/files/users/'.$product->pic)}}" />
                                    <h2 class="col-7">{{$product->name}}</h2>
                                </div>
                            @endforeach
                    </div>
                </div>
            </div>
        @else
            <div class="text-center">
                No such User!
            </div>
        @endif
    </div>
@endsection
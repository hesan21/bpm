@extends('admin.layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header text-center">Products Options</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row text-center offset-2">
                        <div class="col-4 p-4 m-2 bg-primary rounded">
                            <a href="{{route('products-approval')}}" class="text-light">Employees Products Approval</a>
                        </div>
                        <div class="col-4 p-4 m-2 bg-dark rounded">
                            <a href="{{route('order-approval')}}" class="text-light">Customer Order Aprroval</a>
                        </div>
                        <div class="col-4 p-4 m-2 bg-success rounded">
                            <a href="{{route('order-status')}}" class="text-light">Orders Delivering Status</a>
                        </div>
                        {{-- <div class="col-4 p-4 m-2 bg-secondary rounded">
                            <a href="" class="text-light">Products Stock</a>                    
                        </div> --}}

                    </div>
                
                        </div>                        
                    </div>
                    
                </div>
            </div>

            <div class="card mt-2 col-12 col-md-10 offset-md-1">
                <div class="card-header text-center col-12">Accounts Option</div>
                <div class="card-body col-10 offset-2">
                    <a href="{{route('manage-user')}}" class="btn btn-outline-primary col-5 col-sm-4">Manage User Accounts</a>
                    <a href="{{route('manage-employee')}}" class="btn btn-outline-primary col-6 col-md-4 ml-3">Manage Employees Accounts</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

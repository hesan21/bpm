@extends('layouts.app')

@section('title')
    Business Process Management
@endsection

@section('content')
    @include('includes.carousel')
    <h2 class="text-center">------ Top Products ------</h2>
    @if(count($products) > 0)
        @include('includes.product-cards')
    @else
        <div class="display-4 text-center text-danger">
            No Product Created
        </div>
    @endif
    <div class="clear-float"></div>
    <div>
        <a href="{{route('products')}}" class="nav-link"><h2 class="text-center my-5">------ See All Products ------</h2></a>
    </div>

    @include('includes.footer')
@endsection

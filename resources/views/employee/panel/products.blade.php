@extends('employee.layouts.auth')

@section('content')
<div class="container col-12 col-sm-9">
    @if (session('status'))
        <div class=" alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">My Products</div>
        <div class="card-body col-10 offset-3 offset-md-4">
            <a href="{{route('emp.add-product', Auth::user()->id)}}" class="btn btn-primary col-6 col-sm-6 col-md-4 ml-3">Add Products</a>
        </div>
    </div>
    
    @if($products->first()!= null)
        <div class="row">
            <div class="col-4 p-4 text-center">Product Name</div>
            <div class="col-1 p-4">Stock </div>
            <div class="col-2 p-4 text-center">Status </div>
        </div>
        @foreach ($products as $product)
        {{-- {{dd($products[1]->status == 'processing')}} --}}
            <div class="row">
                <a class="col-4 text-center" href="{{route('emp.view-product', $product->id)}}">{{$product->name}}</a>
                <p class="col-1 mr-5 badge badge-{{$product->quantity<1 ? 'danger' : 'primary'}} ">{{$product->quantity}}</p>
                <p class="col-1 mr-5 badge badge-{{$product->status == "processing" ? 'warning' :'success'}} {{$product->status == "rejected" ? text-danger : ''}}">{{$product->status}}</p>

                <div class="btn-group ml-5">
                    <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                    <ul class="dropdown-menu py-0">
                        <li><a class="btn btn-primary col-12 rounded-0" href="{{route('emp.view-product', $product->id)}}">Add Stock</a></li>
                        <li><a class="btn btn-secondary col-12 rounded-0" href="{{route('emp.view-product', $product->id)}}">View Product</a></li>
                        <li><a class="btn btn-dark col-12 rounded-0" href="{{route('emp.remove-product', $product->id)}}">Remove Product</a></li>
                        <li><a class="btn btn-info col-12 rounded-0" href="{{route('emp.update-product', $product->id)}}">Update Product</a></li>
                    </ul>
                </div>
            </div> 
            <hr>   
        @endforeach
    @else
        <div class="mt-1 alert alert-danger text-center">NO Products</div>
    @endif
</div>
@endsection         
@extends('employee.layouts.auth')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header text-center">
                My Orders
            </div>
            @if (session('status'))
                <div class=" alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
            @if(isset($orders) && (count($orders) != 0))
                @foreach($orders as $order)
                    {{-- {{dd($order->products)}} --}}
                    <ul class="list-group-items col-4 mt-2">
                        <li class=" list-group-item">
                            <h5 class="d-inline-block"><strong>Order No: {{$order->order_no}}</strong></h5>
                            <a href="{{route('emp.deliver', $order->order_no)}}"class="d-inline-block btn btn-primary ml-3">Deliver</a>
                        </li>
                        @foreach ($order->products as $item)
                            @foreach($item as $product)
                                <li class="list-group-item">
                                    {{$product['name']}}
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                        
                @endforeach
            @else
                    <div class="text-center text-danger">No Orders!</div>
            @endif
            </div>
        </div>
    </div>
@endsection

@extends('employee.layouts.auth')

@section('content')
<div class="container">
    @if(session('status'))
        <div class="alert alert-danger">{{session('status')}}</div>
    @endif
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <div class="card">
                <div class="card-header">Add Shop</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('shop.register') }}">
                            @csrf
    
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Shop Name') }}</label>
    
                                <div class="col-md-6">
                                    <input id="name" type="text" placeholder="Shop Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
    
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="openhr" class="col-md-4 col-form-label text-md-right">{{ __('Open hour') }}</label>
    
                                <div class="col-md-6">
                                    <input id="openhr" type="time" class="form-control @error('openhr') is-invalid @enderror" name="openhr" value="{{ old('openhr') }}" required autocomplete="openhr">
    
                                    @error('openhr')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="closehr" class="col-md-4 col-form-label text-md-right">{{ __('Close Hour') }}</label>
    
                                <div class="col-md-6">
                                    <input id="closehr" type="time" class="form-control @error('closehr') is-invalid @enderror" name="closehr" required autocomplete="new-closehr">
    
                                    @error('closehr')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <input type="hidden" name="emp_id" value="{{Auth::user()->id}}">
    
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register Shop') }}
                                    </button>
                                </div>
                            </div>
                        </form>
@endsection                    
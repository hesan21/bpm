@extends('employee.layouts.auth')

@section('content')
    <div class="container">
        @if(Session::has('status'))
            <div class="alert alert-success">{{session('status')}}</div>
        @endif
        <h2 class="mb-3 text-center">
            Update Product Details
        </h2>
        <form method="POST" action="{{ route('emp.update-product.submit', $product->id) }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Product Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" placeholder="Product Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ? old('name') : $product->name}}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                    <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('Product Category') }}</label>

                    <div class="col-md-6">
                        <input id="category" type="text" placeholder="Product category" class="form-control @error('category') is-invalid @enderror" name="category" value="{{ old('category') ? old('category') : $product->category}}" required autocomplete="category" autofocus>

                        @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

            <div class="form-group row">
                    <label for="details" class="col-md-4 col-form-label text-md-right">{{ __('Product Details') }}</label>

                    <div class="col-md-6">
                        <input id="details" type="text" placeholder="Product details" class="form-control @error('details') is-invalid @enderror" name="details" value="{{ old('details') ? old('details') : $product->details}}" required autocomplete="details" autofocus>

                        @error('details')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            
            <div class="form-group row">
                <label for="pic" class="col-md-4 col-form-label text-md-right">{{ __('Product Pic') }}</label>
                <div class="col-md-6">
                    <input type="file" name="pic" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('Product Quantity') }}</label>

                <div class="col-md-6">
                    <input id="quantity" type="number" placeholder="Product Quantity" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{ old('quantity') ? old('quantity') : $product->quantity}}" required autocomplete="quantity" autofocus>

                    @error('quantity')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Product price') }}</label>

                    <div class="col-md-6">
                        <input id="price" type="number" placeholder="Product price" class="form-control @error('price') is-invalid @enderror" name="price" value="{{old('price') ? old('price') : $product->price}}" required autocomplete="price" autofocus>

                        @error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

            {{-- <input type="hidden" name="status" value="processing">
            <input type="hidden" name="emp_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="res_id" value="{{$res_id}}"> --}}

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Update Product') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
@extends('employee.layouts.auth')

@section('content')
    <div class="container">
        @if(Session::has('status'))
            <div class="alert alert-success">{{session('status')}}</div>
        @endif
        <div class="card-group">
            <div class="card col-12 col-md-4">
                <img class="w-100 h-100" src="{{asset('/files/users/'.$product->pic)}}" alt="">
            </div>
            <div class="card col-12 col-md-6">
                <h2 class="display-4">{{$product->name}}</h2>
                <p><b>Details: </b>{{$product->details}}</p>
                <p><b>Available Stock:</b> {{$product->quantity}}</p>
                <p><b>Price:</b> {{$product->price}}</p>
                <form action="{{route('emp.product.addStock', $product->id)}}" method="get">
                    @csrf
                    <input type="number" name="stock" required>
                   
                    <button type="submit" class="btn btn-success">Increase Stock</button>
                </form>

                <a class="btn btn-danger mt-2" href="{{route('emp.remove-product', $product->id)}}">Remove Product</a>

            </div>
        </div>
    </div>
@endsection
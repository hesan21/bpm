@extends('employee.layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   
                    <div class="card-deck mx-auto col-10">
                            <div class="card text-white bg-primary mb-3 col-12 col-md-4">
                                <a href="{{route('emp.add-shop', Auth::user()->id)}}" class="text-white">
                                    <div class="card-body">
                                        <p class="card-text text-bottom">Add Shop</p>
                                    </div>
                                </a>
                            </div>

                            <div class="card text-white bg-dark mb-3 col-12 col-md-4">
                                <a href="{{route('emp.show-product', Auth::user()->id)}}" class="text-white">
                                    <div class="card-body">
                                        <p class="card-text">My Products</p>
                                    </div>
                                </a>
                            </div>
    
                            {{-- <div class="card text-white bg-info mb-3 col-12 col-md-4">
                                <a href="" class="text-white">
                                    <div class="card-body">
                                        <p class="card-text">Make Discount Coupons</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        
                        <div class="card-deck mx-auto col-10">
                            <div class="card text-white bg-secondary mb-3 col-12 col-md-4">
                                <a href="" class="text-white">
                                    <div class="card-body">
                                    <p class="card-text">My Wallet</p>
                                    <h3 class="card-title float-right">5</h3>
                                </a>
                            </div>
                        </div> --}}
    
                        <div class="card text-white bg-success mb-3 col-12 col-sm-4">
                            <a href="{{route('emp.orders')}}" class="text-white">
                                <div class="card-body">
                                    <p class="card-text">My Orders</p>
                                </div>
                            </a>
                        </div>
    
                        {{-- <div class="card text-white bg-primary mb-3 col-12 col-sm-4">
                                <a href="" class="text-white">
                                    <div class="card-body">
                                    <p class="card-text">All Orders</p>
                                    <h3 class="card-title float-right">5</h3>
                                </a>
                        </div> --}}
                    </div>                        
                </div>
            </div>
        </div>
    
        {{-- <div class="card mt-2 col-12 col-md-10">
            <div class="card-header text-center">Accounts Option</div>
            <div class="card-body col-10 offset-3 offset-md-4">
                <a href="" class="btn btn-primary col-6 col-sm-4 ml-3">Manage My Account</a>
            </div>
        </div> --}}
    </div>
                
@endsection

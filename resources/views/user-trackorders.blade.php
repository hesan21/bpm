@extends('layouts.app')

@section('title')
    My Dashboard
@endsection
    <?php 
        if($order->order_status == "processing"){
            $Processing = 'text-secondary';
            $Preparing = '';
            $Delivering = '';
            $Completed = '';
        }elseif ($order->order_status == "preparing") {
            $Processing = 'text-success';
            $Preparing = 'text-secondary';
            $Delivering = '';
            $Completed = '';
        }elseif ($order->order_status == "delivering") {
            $Processing = 'text-success';
            $Preparing = 'text-success';
            $Delivering = 'text-secondary';
            $Completed = '';
        }elseif ($order->order_status == "canceled") {
            $Processing = 'text-secondary';
            $Preparing = 'text-secondary';
            $Delivering = 'text-secondary';
            $Completed = 'text-secondary';
        }else {
            $Processing = 'text-success';
            $Preparing = 'text-success';
            $Delivering = 'text-success';
            $Completed = 'text-success';
        }
    ?>
@section('content')
@include('includes.user-dash')
        <div class="card col-md-9">
            <div class="card-header text-center">
                Track Order
            </div>
            @if($order->order_status == 'canceled')
                <div class="alert alert-danger text-center">
                    Order Canceled!!
                </div>
            @endif
            <div class="card-body">
                <ul class="list-group-items d-none d-md-block">
                    <li class="list-group-item border-0">
                    <i class="d-inline-block far fa-check-circle fa-5x mr-5 {{$Processing}}"></i>
                        <i class="d-inline-block far fa-check-circle fa-5x mr-5 {{$Preparing}}"></i>
                        <i class="d-inline-block far fa-check-circle fa-5x mr-5 {{$Delivering}}"></i>
                        <i class="d-inline-block far fa-check-circle fa-5x mr-5 {{$Completed}}"></i>
                    </li>
                    <li class="list-group-item border-0">
                        <p class="d-inline-block mr-5">Processing</p>
                        <p class="d-inline-block mr-5">Preparing</p>
                        <p class="d-inline-block mr-5">Delivering</p>
                        <p class="d-inline-block mr-5">Completed</p>
                    </li>
                    <li class="list-group-item border-0">
                        <h5><strong>Order No:</strong> {{$order->order_no}}</h5>
                    </li>
                    <li class="list-group-item border-0">
                        <h5><strong>Payment:</strong> Rs.{{$order->price}}</h5>
                    </li>
                </ul>

                <ul class="list-group-items d-md-none">
                    <li class="list-group-item border-0">
                        <i class="d-inline-block far fa-check-circle fa-5x mr-5 {{$Processing}}"></i>                    
                        <p class="d-inline-block mr-5">Processing</p>
                        <i class="d-inline-block far fa-check-circle fa-5x mr-5 {{$Preparing}}"></i>
                        <p class="d-inline-block mr-5">Preparing</p>

                        <i class="d-inline-block far fa-check-circle fa-5x mr-5 {{$Delivering}}"></i>
                        <p class="d-inline-block mr-5">Delivering</p>

                        <i class="d-inline-block far fa-check-circle fa-5x mr-5 {{$Completed}}"></i>
                        <p class="d-inline-block mr-5">Completed</p>

                    </li>
                    <li class="list-group-item border-0">
                        <h5><strong>Order No:</strong> {{$order->order_no}}</h5>
                    </li>
                    <li class="list-group-item border-0">
                        <h5><strong>Payment:</strong> Rs.{{$order->price}}</h5>
                    </li>
                </ul>
                
            </div>
        </div>
    </div>
</div>
    
@endsection
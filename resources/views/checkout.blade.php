  
@extends('layouts.app')

@section('title')
    Checkout
@endsection

@section('content')
    <div class="row">
        <div class="mt-2 col-sm-6 col-md-4 offset-sm-4">
            <h1>Checkout</h1>
            <h4>Total Amount: Rs {{ $total }}</h4>
            @if(session::has('error'))
                <div id="charge-error" class="alert alert-danger text-center {{ !Session::has('error') ? 'hidden' : ''  }}">
                    {{ Session::get('error') }}
                </div>
            @endif
            <form action="{{ route('checkout') }}" method="post" id="checkout-form">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" placeholder="Enter Name" required>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" id="address" name="address" value="{{ old('address') }}" class="form-control @error('address') is-invalid @enderror" placeholder="Enter Address" required>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <hr>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="card-name">Card Holder Name</label>
                            <input type="text" id="card-name" name="card_name" value="{{ old('card_name') }}" class="form-control @error('card_name') is-invalid @enderror"  placeholder="E.g: Alex Carey" required>
                            @error('card_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="card-number">Credit Card Number</label>
                            <input type="text" id="card-number" name="card_num" minLength="16" maxlength="16" class="form-control @error('card_num') is-invalid @enderror" placeholder="1234-xxxx-xxxx-xxxx" required>
                            @error('card_num')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="card-expiry-month">Expiration Month</label>
                                    <input type="number" name="expiry_month" id="card-expiry-month" min="1" max="12" placeholder="MM" class="form-control @error('expiry_month') is-invalid @enderror" required>
                                </div>
                                @error('expiry_month')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="card-expiry-year">Expiration Year</label>
                                    <input type="number" name="expiry_year" id="card-expiry-year" min="19" max="25" placeholder="YY" class="form-control @error('expiry_year') is-invalid @enderror" required>
                                    @error('expiry_year')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="card-cvc">CVC</label>
                            <input type="text" name="cvc" maxlength="3" id="card-cvc" placeholder="E.g: 123" class="form-control @error('cvc') is-invalid @enderror" required>
                            @error('cvc')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Buy now</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="{{ URL::to('src/js/checkout.js') }}"></script>
@endsection
@extends('layouts.app')

@section('title')
    Products
@endsection

@section('content')
    <div class="container head-image-products">
        <div class="centered"><h1 class="text-light px-2 py-2">All Products</h1></div>
    </div>

    @if(session('message'))
        <div class="container mt-2">
            <div class="alert alert-success text-center">{{session('message')}}</div>
        </div>
    @endif

    <div class="grid" id="mycard">
    @if(!$products->isEmpty())
        @foreach ($products as $product)
            <figure class="effect-zoe">
                <img src="{{asset('/files/users/'.$product->pic)}}" />
                <figcaption>
                    <h2>{{$product->name}}</h2>
                    <p class="icon-links">
                        @include('includes.add-to-cart')</a>
                    <a href="{{route('product.view', $product->id)}}" ><i class="far fa-eye px-2"></i></a>
                    </p>
                    <p class="description">
                        {{$product->details}}
                    </p>
                </figcaption>			
            </figure>
        @endforeach
    @else
        <div class="text-center display-4 text-danger">
            No Product
        </div>
    @endif
</div>
    <div class="clear-float"></div>
    @include('includes.footer')
@endsection
@extends('layouts.app')

@section('content')
<div class="mt-3 container">
        
        <div class="mt-3 card-deck">
            <div class="card col-12 col-md-3 h-100">
                <img width="100%" src="{{asset('/files/default-shop.jpeg')}}"/>
                <h2 class="text-center mt-2">
                    {{$rest->name}}
                </h2>
            </div>    

            <div class="card col-12 col-md-9">
                    <div class="card-header text-center">
                        Products
                    </div>
                    @if(!$products->isEmpty())
                    <div class="card-body">
                        <ul class=" px-0 list-group-items">
                            @foreach ($products as $product)
                                <li class="list-group-item">
                                    <a href="{{route('product.view', $product->id)}}">
                                        {{$product->name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @else
                        <h4 class="mt-2 text-danger text-center">
                            No Products
                        </h4>
                    @endif
                </div>
            </div>
        </div>
@endsection
@extends('layouts.app')

@section('title')
    Contact
@endsection

@section('content')
    <div class="container mt-4">
        <h2 class="text-center">Contact US</h2>
        @if(Session::has('message'))
            <div class="alert alert-success">{{session('message')}}</div>
        @endif
        <form action="{{route('contact-send')}}" method="POST" class="col-md-8 offset-md-3">
            @csrf
                <div class="form-group w-75">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>

                <div class="form-group w-75">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="contact-name" class="form-control" aria-describedby="emailHelp" placeholder="Enter name" required>
                </div>

                <div class="form-group w-75">
                        <label for="message">Message: </label><br>
                        <textarea name="message" id="contact-message" class="form-control" placeholder="Enter Your Message" required></textarea>
                </div>
                
                
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
    </div>
<div class="mt-5"></div>
    @include('includes.footer')
@endsection
@extends('layouts.app')

@section('title')
    My Dashboard
@endsection

@section('content')
    @include('includes.user-dash')
        <div class="card col-12 col-md-9">
            <div class="card-header text-center">
                My Orders
            </div>

            @if(Session::has('status'))
            <div class="alert alert-danger text-center mt-2">
                {{session('status')}}
            </div>
            @endif
            @if (isset($orders) && !$orders->isEmpty())
            <div class="card-body">
                <ul class="px-0 list-group-items">
                    <li class="list-group-item">
                        <p class="col-3 d-inline-block"><strong> Order No</strong></p>
                        <p class="col-2 d-inline-block"><strong> Price </strong></p>
                        <p class="col-3 d-inline-block"><strong> Status</strong></p>
                        <p class="col-3 d-inline-block"><strong> Cancel <small>(if not processed)</small></strong></p>
                    </li>
                
                
                        @foreach ($orders as $order)
                        <li class="list-group-item">
                            <a href="{{route('user.trackorders', $order->order_no)}}">
                                <p class="col-3 d-inline-block">{{$order->order_no}}</p>
                            </a>
                            <p class="col-2 d-inline-block">Rs. 500</p>
                            <p class="col-3 text-center d-inline-block rounded bg-info">{{$order->order_status}}</p>
                            @if($order->order_status == 'processing')
                                <a href="{{route('user.cancelorder', $order->order_no)}}" class="col-3 btn btn-danger">Cancel Order</a>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
            @else
                <div class="card-body text-center text-danger">
                    No Order Placed Yet!
                </div>
            @endif

        </div>
    </div>
</div>
    
@endsection
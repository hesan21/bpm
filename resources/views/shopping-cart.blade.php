@extends('layouts.app')

@section('title')
    Shopping Cart
@endsection

@section('content')
<div class="clearfix"></div>

@if(session('message'))
    <div class="col-10 container mt-2">
        <div class=" alert alert-success text-center">
            {{session('message')}}
        </div>
    </div>
@endif

<div class="container">

    @if (Session::has('cart'))
        <div class="row mt-2">
            <div class="col-12 col-lg-10 offset-lg-1">
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong class=" col-1">Quantity</strong>
                        <strong class="col-1 offset-md-1 offset-md-1 pl-lg-5">Product Name</strong>
                        <strong class="col-2 offset-md-2 offset-lg-2 pl-lg-5">Price</strong>
                        <div class="float-right pr-3">
                            <strong>Actions</strong>
                        </div>
                    </li>  
                    @foreach($products as $product)
                    
                        <li class="list-group-item ">
                            <span class="badge col-1 offset-1 offset-sm-0 text-left text-sm-right">{{$product['qty']}}</span>
                            <strong class="col-6 offset-1 offset-sm-2">{{$product['item']['name']}}</strong>
                            <span class=" col-2 offset-1 offset-md-3 pl-lg-3">{{$product['price']}}</span>
                            <div class="btn-group float-right">
                                <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu py-0">
                                    <li><a href="{{route('product.reduceByOne', $product['item']['id'])}}" class="btn btn-danger col-12 rounded-0">Remove one Item</a></li>
                                    <li><a href="{{route('product.remove', $product['item']['id'])}}" class="btn btn-danger col-12 rounded-0">Remove All</a></li>
                                </ul>
                            </div>
                        </li>    
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="mt-2 col-4 offset-8 offset-sm-8 mb-2">
                <strong>Total Price: {{$totalPrice}}</strong>
            </div>
        </div>
        <div class="row">
            <div class=" col-4 offset-8 offset-sm-8">
                <a href="{{route('checkout')}}" class="btn btn-success">Checkout</a>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-sm-6 offset-sm-3 mt-4">
                <h2 class="text-center text-danger">No Item in the cart!</h2>
            </div>
            <div class="col-sm-12 text-center">
                <a href="{{route('products')}}" class="btn btn-success ">Find Products</a>
            </div>
        </div>
    @endif
</div>
@endsection
@extends('layouts.app')

@section('title')
    Shop
@endsection

@section('content')
    <div class="container head-image-shops">
        <div class="centered"><h1 class=" text-light px-2 py-2">All Shops</h1></div>
    </div>

    <div class="grid" id="mycard">
        @if(!$shops->isEmpty())
            @foreach ($shops as $shop)
                <figure class="effect-zoe">
                    <img src="{{asset('/files/default-shop.jpeg')}}"/>
                    <figcaption>
                        <h2>{{$shop->name}}</h2>
                        <button><a href="{{route('shops-products', $shop->id)}}" class="btn btn-sm btn-success"> Products</a></button>
                    </figcaption>			
                </figure>
            @endforeach
        @else
        <div class="row">
            <div class="col-sm-6 offset-sm-3 mt-4">
                <h2 class="text-center text-danger">No Shop Registered!</h2>
            </div>
        </div>
        @endif
        
    </div>
    <div class="clear-float"></div>
    @include('includes.footer')
@endsection
<div class="mt-3 container">
    <h2 class="text-center ">
        My Dashboard
    </h2>
    @if(Session::has('message'))
        <div class="alert alert-danger text-center">{{session('message')}}</div>
    @endif
    <div class="mt-3 card-deck">
        <div class="card col-12 col-md-3 h-100">
            <ul class="navbar-nav">
                <li class="nav-item py-2 text-center"><a href="{{route('user.account', Auth::user()->id)}}">Account Details</a> </li>
                <li class="nav-item py-2 text-center"><a  href="{{route('user.orders')}}">My Orders</a> </li>
                {{-- <li class="nav-item py-2 text-center"><a href="{{route('user.trackorders')}}">Track my Order</a></li> --}}
                <li class="nav-item py-2 text-center"><a href="{{route('user.changepasswordform')}}">Change Password</a></li>
            </ul>
        </div>
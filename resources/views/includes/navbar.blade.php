<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
  <div class="container mt-2">
    <a class="navbar-brand mr-5" href="/">Business Process Management</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample04">
        <ul class="navbar-nav mr-auto ml-5">
            <li class="nav-item ">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href={{route('shops.view')}}>Shops</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href={{route('products')}}>Products</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href={{route('contact')}}>Contact</a>
            </li>
            <li class="nav-item mr-5">
                <a class="nav-link" href="{{route('about')}}"> About</a>
            </li>

            {{-- <form role="search" id="search">
                <div class="row">
                    <input type="text" class="form-control col-8 col-sm-9" placeholder="Search">
                    <button type="submit" class="form-control col-2 col-sm-2 form-control-submit"><i class="fas fa-search"></i></button>
                </div>
            </form> --}}
            
            
        </ul>
        
        <div class="navbar-right">
            <ul class="navbar-nav ml-5">
                @if(Auth::guard('web')->guest())
                    <li class="nav-item mr-1"><a class="nav-link" href="{{route('login')}}"><i class="fas fa-user"></i></a></li>
                    <li class="nav-item mr-1"><a class="nav-link" href="{{route('register')}}"><i class="fas fa-user-plus"></i></a></li>
                    <li class="nav-item mr-3"><a class="nav-link" href="{{route('product.shoppingCart')}}"><i class="fas fa-shopping-cart"></i>
                        <span class="badge">{{Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span>
                    </a></li>
                @else
                    {{-- <li class="nav-item"><a class="nav-link" href="{{route('home')}}"><i class="fas fa-user"></i></a></li>
                    <li class="nav-item mr-4 col-1"><form id="logout-form" action="{{ route('user-logout') }}" method="get">
                        @csrf
                        <button class="btn" type="submit"><i class="fas fa-sign-out-alt"></i></button>
                    </form></li> --}}
                    <li class="nav-item mr-3"><a class="nav-link" href="{{route('product.shoppingCart')}}"><i class="fas fa-shopping-cart"></i>
                        <span class="badge">{{Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span>
                    </a></li>
                    <ul class="row navbar-nav dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                           <img style="width:30px; border-radius:25%;" src="{{asset('files/users/'.Auth::user()->pic)}}" alt="">
                           {{ Auth::user()->name }} 
                           <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('user.dashboard', Auth::user()->id)}}">
                                My Dashboard
                            </a>
                            <a class="dropdown-item"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('user-logout') }}" method="GET" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </ul>
                @endif
                
            </ul>
    </div>
  </div>
</nav>

<div class="clearfix"></div>

<div class="grid w-100">
        @if(isset($products))
        @foreach ($products as $product)
            <figure class="effect-zoe">
                <img src="{{asset('/files/users/'.$product->pic)}}" />
                <figcaption>
                    <h2>{{$product->name}}</h2>
                    <p class="icon-links">
                        @include('includes.add-to-cart')</a>
                    <a href="{{route('product.view', $product->id)}}" ><i class="far fa-eye px-2"></i></a>
                    </p>
                    <p class="description">{{$product->details}}</p>
                </figcaption>			
            </figure>
        @endforeach
    @else
        <div class="container">
            No Product At the Moment!
        </div>
    @endif
</div>
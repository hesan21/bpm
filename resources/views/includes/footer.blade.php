<footer class="bg-dark py-3">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 footer-contact float-left ">
                    <h4 class="text-light">CONTACT US</h4>
                    <ul class="navbar-nav ">
                        <li class="text-light">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <a href="#" class="tran3s ">testemail@gmail.com</a>
                        </li>
                        <li class="text-light">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <a class="tran3s">+92-42-09007860-1</a>
                        </li>
                    </ul>
                </div> <!-- /.footer-contact -->

                <div class="col-md-4 col-sm-6 col-xs-12 footer-quick-link">
                    <h4 class="text-light">Quick link</h4>
                    <ul class="navbar-nav">
                    <li class="text-light"><a class="text-light" href="{{route('index')}}" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Home</a></li>
                        <li class="text-light"><a class="text-light" href="{{route('products')}}" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Products</a></li>
                        <li class="text-light"><a class="text-light" href="{{route('about')}}" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> About</a></li>
                        <li class="text-light"><a class="text-light" href="{{route('shops.view')}}" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Shops</a></li>                        
                        <li class="text-light"><a class="text-light" href="{{route('contact')}}" class="tran3s"><i class="fa fa-caret-right" aria-hidden="true"></i> Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="bottom-footer text-center bg-light mt-2">
        <p>Copyright 2019 &copy; <a href="#" class="tran3s" target="_blank">Business Process Management</a></p>
    </div> 
</footer>
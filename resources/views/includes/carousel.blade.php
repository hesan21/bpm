<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="{{asset('imgs/carousel/burger.jpeg')}}" alt="First slide">
        <div class="container">
          <div class="carousel-caption text-left">
            <h1>Delicious Burgers.</h1>
            <p>Eat clean to stay fit, have a burger to stay sane</p>
          <p><a class="btn btn-lg btn-primary" href="{{route('products')}}" role="button">See all Products</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        {{-- <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" ><rect fill="#777" width="100%" height="100%"/></svg> --}}
        <img class="d-block w-100" src="{{asset('imgs/carousel/image2.jpeg')}}" alt="First slide">        
        <div class="container">
          <div class="carousel-caption text-right">
              <h1>Pizza for Life.</h1>
              <p>THERE’S NO BETTER FEELING IN THE WORLD THAN A WARM PIZZA BOX IN YOUR LAP.</p>
              <p><a class="btn btn-lg btn-primary" href="{{route('products')}}" role="button">Order Your Product!</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        {{-- <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" ><rect fill="#777" width="100%" height="100%"/></svg> --}}
        <img class="d-block w-100" src="{{asset('imgs/carousel/image3.jpeg')}}" alt="First slide">        
        <div class="container">
          <div class="carousel-caption mb-5">
              <h1>Variety of Food</h1>
              <p>One cannot think well, love well, sleep well, if one has not dined well</p>
              <p><a class="btn btn-lg btn-primary" href="{{route('products')}}" role="button">Order Your Meal!</a></p>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

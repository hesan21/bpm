<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->time('open_hours');
            $table->time('close_hours');
            
            $table->bigInteger('user_id')->unsigned();

            $table->bigInteger('emp_id')->unsigned();
            $table->bigInteger('admin_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('emp_id')->references('id')->on('employees');
            $table->foreign('admin_id')->references('id')->on('admins');     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}

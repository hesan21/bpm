<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainPagesController@index')->name('index');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');


Route::get('view-product/{id}', 'MainPagesController@viewProduct')->name('product.view');
Route::get('shops', 'MainPagesController@viewShops')->name('shops.view');
Route::post('contact-us', 'MainPagesController@sendMail')->name('contact-send');
Route::get('/shop/{id}/products', 'MainPagesController@shopsProduct')->name('shops-products');


Route::get('/products', 'ProductsController@index')->name('products');
Route::get('/product', 'ProductsController@index')->name('product.back');
Route::get('/add-to-cart/{id}', 'ProductsController@addToCart')->name('product.addToCart');
Route::get('/reduce/{id}', 'ProductsController@getReduceByOne')->name('product.reduceByOne');
Route::get('/remove/{id}', 'ProductsController@getRemoveItem')->name('product.remove');
Route::get('/shopping-cart', 'ProductsController@getCart')->name('product.shoppingCart');
Route::get('/checkout', 'ProductsController@getcheckout')->name('checkout')->middleware('auth');


Route::post('/checkout', 'HomeController@postcheckout')->name('checkout');
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('user/logout', 'Auth\LoginController@userlogout')->name('user-logout');
Route::get('user/dashboard/{id}', 'HomeController@accountdetails')->name('user.dashboard');
Route::get('user/all-orders', 'HomeController@userorders')->name('user.orders');
Route::get('user/track-order/{id}', 'HomeController@trackorders')->name('user.trackorders');
Route::get('user/changepassword', 'HomeController@changepassform')->name('user.changepasswordform');
Route::get('user/accountdetails/{id}', 'HomeController@accountdetails')->name('user.account');
Route::post('user/accountdetails/update', 'HomeController@updatedetails')->name('user.update');
Route::post('user/changepasswordsubmit', 'HomeController@changepass')->name('user.changepasssubmit');
Route::get('user/cancelorder/{id}', 'HomeController@cancelOrder')->name('user.cancelorder');


Auth::routes(['verify' => true]);

Route::prefix('employee')->group(function(){

    Route::get('/', 'EmployeesController@index')->name('employee-home');
    Route::get('/register', 'Auth\EmployeesRegisterController@showRegisterForm')->name('employee-register');
    Route::post('/register', 'Auth\EmployeesRegisterController@register')->name('employee-register.submit');
    Route::get('/login', 'Auth\EmployeeLoginController@showLoginForm')->name('employee.login');
    Route::post('/login', 'Auth\EmployeeLoginController@login')->name('employee.login.submit');
    Route::post('/logout', 'Auth\EmployeeLoginController@logout')->name('employee.logout');

    Route::post('/password/email', 'Auth\EmployeeForgotPasswordController@sendResetLinkEmail')->name('emp.password.email');
    Route::get('/password/reset', 'Auth\EmployeeForgotPasswordController@showLinkRequestForm')->name('emp.password.request');
    Route::post('/password/reset', 'Auth\EmployeeResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\EmployeeResetPasswordController@showResetForm')->name('emp.password.reset');

    Route::get('/add-shop/{id}', 'PagesController@addShop')->name('emp.add-shop');
    Route::get('/show-product/{id}', 'PagesController@showproduct')->name('emp.show-product');
    Route::get('/add-products/{id}', 'PagesController@addproduct')->name('emp.add-product');
    Route::post('/store-products', 'PagesController@storeproduct')->name('emp.add-product.submit');
    Route::get('/view-products/{id}', 'PagesController@viewproduct')->name('emp.view-product');
    Route::get('/update-products/{id}', 'PagesController@editproduct')->name('emp.update-product');
    Route::post('/update-product/{id}', 'PagesController@updateproduct')->name('emp.update-product.submit');
    Route::get('/Deliver/{id}', 'PagesController@deliver')->name('emp.deliver');

    Route::post('/shop-register', 'PagesController@registerShop')->name('shop.register');
    route::get('/add-stock/{id}', 'PagesController@addStock')->name('emp.product.addStock');
    Route::get('/remove-product/{id}', 'PagesController@removeProduct')->name('emp.remove-product');
    Route::get('/my-orders', 'PagesController@showOrders')->name('emp.orders');
});

Route::prefix('admin')->group(function(){
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/register', 'Auth\AdminRegisterController@showRegisterForm')->name('admin.register');
    Route::post('/register', 'Auth\AdminRegisterController@register')->name('admin.register.submit');
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/manage-users', 'Auth\AdminActionsController@users')->name('manage-user');
    Route::get('/manage-employees', 'Auth\AdminActionsController@employees')->name('manage-employee');
    Route::get('/manage-users/{id}', 'Auth\AdminActionsController@showuser')->name('user-manage');
    Route::get('/manage-employees/{id}', 'Auth\AdminActionsController@showemployee')->name('employee-manage');
    Route::get('/products-approvals', 'Auth\AdminActionsController@showapproval')->name('products-approval');
    Route::get('approval-product/{id}', 'Auth\AdminActionsController@viewProduct')->name('admin.product.view');
    Route::get('order-view/{id}', 'Auth\AdminActionsController@orderview')->name('admin.order.view');

    Route::get('/product-approved/{id}', 'Auth\AdminActionsController@productapproved')->name('product-approved');
    Route::get('/product-reject/{id}', 'Auth\AdminActionsController@productreject')->name('product-reject');
    Route::get('/orders-approval', 'Auth\AdminActionsController@showApprovals')->name('order-approval');
    Route::get('/order-approved/{id}', 'Auth\AdminActionsController@orderapproved')->name('order-approved');
    Route::get('/order-reject/{id}', 'Auth\AdminActionsController@orderreject')->name('order-reject');
    Route::get('/orders-status', 'Auth\AdminActionsController@orderstatus')->name('order-status');
    Route::get('/orders-complete/{id}', 'Auth\AdminActionsController@ordercomplete')->name('order-complete');
    Route::get('/manage-user/{id}', 'Auth\AdminActionsController@deleteuser')->name('admin.deleteuser');
    Route::get('/manage-employee/{id}', 'Auth\AdminActionsController@deleteemployee')->name('admin.deleteemp');

    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

});

